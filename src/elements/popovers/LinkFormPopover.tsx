import React, { useState } from 'react'
import { WysiwygJsEditor } from '../../wysiwygjs/WysiwygJsEditor'

interface Props {
  wysiwygEditor: WysiwygJsEditor
}

export function LinkFormPopover({ wysiwygEditor }: Props) {
  const [url, setUrl] = useState('')
  const [text, setText] = useState('')

  return (
    <div className="wysiwyglinkform" tabIndex={1}>
      <div>
        <input
          id="wysiwygklinkurl"
          placeholder="LINK ADDRESS"
          value={url}
          onChange={(event) => setUrl(event.target.value)}
          autoFocus
        />
      </div>
      <div>
        <input
          id="wysiwygklinktext"
          placeholder="(OPTIONAL) TEXT"
          value={text}
          onChange={(event) => setText(event.target.value)}
        />
      </div>
      <button onClick={() => wysiwygEditor.addLink(url, text)} className="addlinkbutton">
        <svg
          id="i-checkmark"
          viewBox="0 0 32 32"
          width="100%"
          height="100%"
          fill="none"
          stroke="currentcolor"
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth="6.25%"
        >
          <path d="M2 20 L12 28 30 4" />
        </svg>
      </button>
    </div>
  )
}
