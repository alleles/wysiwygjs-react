export type ReferencesByCategory = { name: string; references: Reference[] }[]

export interface Reference {
  authors?: string
  title?: string
  year?: string
}
