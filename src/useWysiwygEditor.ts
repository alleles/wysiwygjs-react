import { useEffect, useState } from 'react'
import { WysiwygJsEditor } from './wysiwygjs/WysiwygJsEditor'

export function useWysiwygEditor({
  containerRef,
  username,
  onEditorObjectChange,
  onContentChange,
  initialValue,
  value,
  disabled,
  onPasteImage,
}) {
  const [editorObject, setEditorObject] = useState<WysiwygJsEditor | null>(null)

  useEffect(() => {
    if (containerRef.current) {
      if (!editorObject) {
        const newEditorObject = new WysiwygJsEditor({
          element: containerRef.current,
          username,
          onEditorObjectChange,
          onContentChange,
          initialValue,
          disabled,
          onPasteImage,
        })

        onEditorObjectChange(newEditorObject)
        setEditorObject(newEditorObject)
      }

      if (editorObject) {
        editorObject.onContentChange = onContentChange
        editorObject.disabled = disabled
        editorObject.editor.readOnly(disabled)
        onEditorObjectChange(editorObject)
        if (value !== undefined && value !== editorObject.editor.getHTML()) {
          editorObject.editor.setHTML(value)
        }
      }
    }
  }, [onContentChange, disabled])
}
