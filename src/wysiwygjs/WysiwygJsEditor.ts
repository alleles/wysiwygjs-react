// @ts-nocheck
import wysiwyg from './wysiwyg'
import { Reference } from '../types/References'
import sanitizeHtml from 'sanitize-html'
import { EventListeners } from './EventListeners'
import { WysiwygPopoverType } from '../Wysiwyg'

export class WysiwygJsEditor {
  currentlyOpenPopover: WysiwygPopoverType | null = null
  fontColors: string[]
  highlightColors: string[]
  popoverAnchorElement: HTMLElement | null = null
  private DEFAULT_COLOR: { HEX: string; RGB: string }
  private buttons: Record<string, HTMLButtonElement>
  private buttonsElement: HTMLElement
  private collapsed: any
  private disabled: boolean
  private editor: any
  private editorElement: HTMLElement
  private isBlurred: boolean
  onContentChange: (newValue: string) => void
  private onEditorObjectChange: (editor: WysiwygJsEditor) => void
  private previewElement: HTMLElement
  private showControls: boolean | undefined
  private source: string
  private sourceMode: boolean
  private username: string
  private onPasteImage: (file: File) => Promise<string>

  constructor({
    element,
    username,
    onEditorObjectChange,
    onContentChange,
    initialValue,
    disabled,
    onPasteImage,
  }) {
    this.username = username
    this.onEditorObjectChange = onEditorObjectChange
    this.onContentChange = onContentChange
    this.containerElement = element
    this.editorElement = element.children[0].children[1]
    this.previewElement = element.children[1]
    this.buttonsElement = element.children[2]
    this.disabled = disabled
    this.onPasteImage = onPasteImage

    this.buttons = {}
    this.showControls = 'showControls' in this ? this.showControls : true

    for (const buttonElement of this.buttonsElement.children) {
      let name = buttonElement.id.split('-')[1]
      this.buttons[name] = buttonElement as HTMLButtonElement
    }

    this.DEFAULT_COLOR = {
      HEX: '#F5F5F9',
      RGB: '245, 245, 249',
    }

    this.fontColors = [
      '#000000', // black
      '#FF0000', // red
      '#00B050', // green
      '#0000FF', // blue
      '#E26B0A', // orange
    ]

    this.highlightColors = [
      this.DEFAULT_COLOR.HEX,
      '#D90309', // red
      '#66FF33', // green
      '#FDFF16', // yellow
      '#0967F9', // blue
    ]

    // this.attachmentResource = AttachmentResource

    this.buttonsElement.hidden = true
    // TODO: Solve preview element. Should be visible when sectionbox is collapsed.
    this.previewElement.hidden = true
    this.isBlurred = true

    this.sourceMode = false
    this.source = ''

    this.setupEditor()
    this.setupEventListeners()
    this.editor.setHTML(initialValue)
    this.disabled && this.editor.readOnly(true)
  }

  setupEditor() {
    const selectedHighlightColorDisplayElement = this.buttons['highlightcolor']
      .children[1] as HTMLSpanElement
    const selectedFontColorDisplayElement = this.buttons['fontcolor']
      .children[0] as HTMLSpanElement

    var options = {
      element: this.editorElement,
      onKeyDown: (key, character, shiftKey, altKey, ctrlKey, metaKey) => {
        // "s" key
        if (altKey && key === 83) {
          this.insertSignature()
          return false
        }
        if (ctrlKey || metaKey) {
          if (character.toLowerCase() === 'b') {
            this.editor.bold()
            return false
          } else if (character.toLowerCase() === 'i') {
            this.editor.italic()
            return false
          } else if (character.toLowerCase() === 'u') {
            this.editor.underline()
            return false
          }
        }
      },
      onSelection: (collapsed, rect, nodes) => {
        // Show color of selected text in highlight/font color buttons
        const getCurrentColors = (nodes) => {
          const getTree = (node) => {
            if (!node) {
              return []
            }
            if (node.nodeName === 'WYSIWYG-EDITOR') {
              return [node]
            }
            return [node].concat(getTree(node.parentElement))
          }
          let highlightcolors: string[] = []
          let fontcolors: string[] = []
          for (let i = 0; i < nodes.length; i++) {
            let subtree = getTree(nodes[i])
            for (let j = 0; j < subtree.length; j++) {
              if (subtree[j].style) {
                if (subtree[j].color) {
                  fontcolors = fontcolors.concat(subtree[j].color)
                } else {
                  fontcolors = fontcolors.concat('rgb(0,0,0)')
                }
                break // Check only first styled element in tree
              }
            }
            for (let j = 0; j < subtree.length; j++) {
              if (subtree[j].style && subtree[j].style['background-color']) {
                if (
                  subtree[j].style['background-color'] !==
                  `rgb(${this.DEFAULT_COLOR.RGB})`
                ) {
                  highlightcolors = highlightcolors.concat(
                    subtree[j].style['background-color'],
                  )
                } else {
                  highlightcolors = highlightcolors.concat(
                    `rgb(${this.DEFAULT_COLOR.RGB})`,
                  ) // default color
                }
                break // Check only first styled element in tree
              }
            }
          }
          return {
            fontcolors: fontcolors,
            highlightcolors: highlightcolors,
          }
        }

        let colors = getCurrentColors(nodes)

        if (colors['highlightcolors'].length === 1) {
          selectedHighlightColorDisplayElement.style.color = colors['highlightcolors'][0]
        } else {
          selectedHighlightColorDisplayElement.style.color = `rgb(${this.DEFAULT_COLOR.RGB})`
        }
        if (colors['fontcolors'].length === 1) {
          selectedFontColorDisplayElement.style.color = colors['fontcolors'][0]
        } else {
          selectedFontColorDisplayElement.style.color = 'rgb(0,0,0)'
        }
      },
    }
    selectedHighlightColorDisplayElement.style.color = `rgb(${this.DEFAULT_COLOR.RGB})`
    selectedFontColorDisplayElement.style.color = 'rgb(0,0,0)'

    this.editor = wysiwyg(options)
  }

  updateViewValue() {
    let s = this.editor.getHTML()
    s = s === '<br>' ? '' : s // fix empty editor returning <br>
    this.onContentChange(s)
  }

  /**
   * Returns function responsible for handling the action triggered by clicking a
   * toolbar button
   *
   * @param {String} actionName Name of action to get function for
   */
  buttonAction(actionName, clickElement?) {
    const actions = {
      bold: this.editor.bold,
      italic: this.editor.italic,
      underline: this.editor.underline,
      monospace: () => this.editor.fontName('monospace'),
      orderedList: () => this.editor.insertList(true),
      unorderedList: () => this.editor.insertList(false),
      heading1: () => this.editor.format('h1'),
      heading2: () => this.editor.format('h2'),
      paragraph: () => this.editor.format('div'),
      removeFormat: () => {
        this.editor.format('div')
        this.editor.removeFormat()
      },
      linkform: () => {
        this.setCurrentlyOpenPopover('linkform', clickElement)
      },
      templates: () => {
        this.setCurrentlyOpenPopover('templates', clickElement)
      },
      references: () => {
        this.setCurrentlyOpenPopover('references', clickElement)
      },
      fontcolor: () => {
        this.setCurrentlyOpenPopover('fontcolor', clickElement)
      },
      highlightcolor: () => {
        this.setCurrentlyOpenPopover('highlightcolor', clickElement)
      },
      src: () => this.toggleSource(),
      signature: () => this.insertSignature(),
    }

    actions[actionName]()
  }

  setCurrentlyOpenPopover(key: WysiwygPopoverType, clickElement) {
    this.currentlyOpenPopover = key
    this.popoverAnchorElement =
      clickElement.tag === 'button' ? clickElement : clickElement.closest('button')
    this.saveSelection()
    this.onEditorObjectChange(this)
  }

  closePopover() {
    this.currentlyOpenPopover = null
    this.onEditorObjectChange(this)
  }

  saveSelection() {
    // Ask editor to store selection
    // it is automatically used inside execCommand
    this.editor.openPopup()
  }

  getTextFromHTML(html) {
    // Ignore all elements (except <img>)
    html = html.replace(/<(?!\s*img)[^>]*>/g, '')
    // Ignore inline comments
    html = html.replace(/<!--[\s\S]*-->/g, '')
    // Ignore all whitespace
    html = html.replace(/s+/g, '')
    return html
  }

  blur() {
    // We cannot blur on every click, since using setHTML()
    // puts other elements in focus. Plus it's more to do on
    // every single click.
    if (!this.isBlurred) {
      this.closePopover()
      this.isBlurred = true
      setTimeout(() => {
        // Set timeout so changes in layout due to removal
        // doesn't disturb what user actually clicked on
        this.buttonsElement.hidden = true
      }, 100)
      // Clean up HTML
      if (!this.disabled) {
        if (this.getTextFromHTML(this.editor.getHTML()) === '') {
          this.editor.setHTML('')
        }

        let html = this.editor.getHTML()
        html = html.replace(`background-color: rgb(${this.DEFAULT_COLOR.RGB});`, '')
        html = html.replace('style=""', '')
        this.editor.setHTML(html)

        // Update ngModel
        this.updateViewValue()
      }
    }
  }

  focus() {
    if (!this.editor.readOnly()) {
      if (this.collapsed !== undefined && this.collapsed) {
      }
      this.isBlurred = false
      this.editorElement.focus()
      this.buttonsElement.hidden = false
    }
  }

  /**
   * Debug function to show html source
   */
  toggleSource() {
    if (this.sourceMode) {
      this.editor.setHTML(this.source)
    } else {
      this.source = this.editor.getHTML()
      this.editor.setHTML(this.source.replace(/</g, '&lt;').replace(/>/g, '&gt;'))
    }
    this.editor.readOnly(!this.sourceMode)
    this.sourceMode = !this.sourceMode
    for (let btn in this.buttons) {
      if (btn !== 'src') {
        this.buttons[btn].disabled = this.editor.readOnly()
      }
    }
  }

  addLink(url: string, text: string) {
    if (!url.trim()) {
      // Empty url
      this.closePopover()
      return
    }
    if (!url.startsWith('http')) {
      // Should start with either http or https
      url = 'http://' + url
    }

    // Get link text
    text = text ?? ''

    // Insert link
    this.editor.insertHTML(
      '<div><a href="' +
        url +
        '" target="' +
        url +
        '"><span>' +
        text +
        '</span></a></div>',
    )
    this.closePopover()
  }

  insertTemplate(template) {
    this.editor.insertHTML(template.template)
    this.closePopover()
  }

  insertSignature() {
    const d = new Date().toISOString().substring(0, 10)
    this.editor.insertHTML(
      `[<span style="color: #0000ff;">${this.username}, ${d}</span>]`,
    )
  }

  insertReference(ref) {
    const formatted = `${ref.authors} (${ref.year}) ${ref.journal}`
    this.editor.insertHTML(formatted)
    this.closePopover()
  }

  formatReference(ref: Reference, withTitle?: string) {
    if (ref) {
      if (withTitle) {
        return `${ref.authors} (${ref.year}): ${ref.title}`
      } else {
        return `${ref.authors} (${ref.year})`
      }
    }
    return ''
  }

  setFontColor(color) {
    this.editor.forecolor(color)
    this.closePopover()
  }

  setHighlightColor(color) {
    this.editor.highlight(color)
    this.closePopover()
  }

  setupEventListeners() {
    let eventListeners = new EventListeners()

    // Blur whenever clicking outside. Use 'mousedown'
    // so that we don't blur when user is selecting text and
    // ending with pointer outside our element
    eventListeners.add(document, 'mousedown', (e) => {
      if (!this.containerElement?.contains(e.target)) {
        setTimeout(() => this.blur())
      }
    })

    // Update model whenever contenteditable input is triggered
    eventListeners.add(this.editorElement, 'input', () => {
      this.updateViewValue()
    })

    // Show ourselves when clicked upon
    eventListeners.add(this.editorElement, 'focus', () => {
      this.focus()
    })

    eventListeners.add(this.previewElement, 'click', () => {
      this.focus()
    })

    // Close all popovers on ESC
    eventListeners.add(document, 'keyup', (e) => {
      if (e.key === 'Escape') {
        setTimeout(() => this.closePopover())
      }
    })

    eventListeners.add(this.editorElement, 'paste', (e: ClipboardEvent) => {
      if (!e.clipboardData?.items.length) return
      if (e.clipboardData.types.indexOf('text/html') > -1) {
        let text = e.clipboardData.getData('text/html')
        e.preventDefault()
        this.editor.insertHTML(sanitize(text))
      } else {
        e.preventDefault()
        for (let item of e.clipboardData.items) {
          if (item.kind === 'file') {
            // Ideally we'd want to do this with a signal, but we can not return anything from a signal
            // without putting it in state. Therefore, we take the shortcut and use Cerebral's http
            // provider directly here to update the content in the editor with link to the uploaded image
            const file = item.getAsFile()
            if (file) {
              this.onPasteImage(file).then((html) => this.editor.insertHTML(html))
            }
          }
        }
      }
    })

    // Add slider to scale image
    // No need to add custom image scaling on firefox (it's already available)
    const isFirefox = 'InstallTrigger' in window
    if (!isFirefox) {
      eventListeners.add(this.editorElement, 'click', (e) => {
        if (!this.editor.readOnly()) {
          if (e.target.tagName !== 'IMG') {
            return
          }

          let img = e.target
          let imgId = img.id

          let currentScale = (1.0 * img.width) / img.naturalWidth
          let minScale = 0.1
          let maxScale = 1.5

          // Create div with slider
          let sliderContainer: HTMLDivElement | null = document.createElement('div')
          sliderContainer.classList.add('image-slider')
          let slider: HTMLInputElement | null = document.createElement('input')
          sliderContainer.appendChild(slider)

          // Specify slider properties
          slider.type = 'range'
          slider.min = minScale.toString()
          slider.max = maxScale.toString()
          slider.step = '0.01'

          const editorRect = this.editorElement.getBoundingClientRect()
          const imgRect = img.getBoundingClientRect()

          const left = imgRect.left - editorRect.left
          const top = imgRect.top - editorRect.top
          sliderContainer.style.left = left + 'px'
          sliderContainer.style.top = top + 'px'

          // Append slider to parent element
          this.editorElement.parentNode?.appendChild(sliderContainer)

          slider.value = currentScale.toString()

          // Focus slider without hiding editors toolbar
          slider.focus()

          // Prevent clicking slider from affecting popovers
          slider.onclick = (e) => {
            e.stopPropagation()
          }

          slider.oninput = () => {
            if (slider) {
              // We have to fetch img again (for some reason)
              let imgElement = document.getElementById(imgId) as HTMLImageElement

              // Scale image proportionally with slider value (height is set to auto)
              imgElement.width = parseFloat(slider.value) * imgElement.naturalWidth
            }
          }

          slider.onblur = () => {
            // Update model
            this.updateViewValue()
            // Remove slider element
            sliderContainer && this.editorElement.parentNode?.removeChild(sliderContainer)
            slider = null
            sliderContainer = null
          }
        }
      })
    }
  }
}

export function sanitize(dirtyHTML) {
  return sanitizeHtml(dirtyHTML, {
    allowedTags: [
      'h1',
      'h2',
      'h3',
      'h4',
      'h5',
      'h6',
      'blockquote',
      'p',
      'a',
      'ul',
      'ol',
      'nl',
      'li',
      'b',
      'i',
      'font',
      'strong',
      'em',
      'strike',
      'u',
      'code',
      'hr',
      'br',
      'div',
      'table',
      'thead',
      'caption',
      'tbody',
      'tr',
      'th',
      'td',
      'pre',
      'span',
    ],
    allowedAttributes: {
      '*': ['style'],
      font: ['color'],
      a: ['href', 'name', 'target'],
    },
    allowedStyles: {
      // Allow basic style options
      '*': {
        // Match HEX and RGB
        color: [
          /^[a-z]+$/,
          /^#(0x)?[0-9a-f]+$/i,
          /^rgb\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*\)$/,
        ],
        'background-color': [
          /^#(0x)?[0-9a-f]+$/i,
          /^rgb\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*\)$/,
        ],
        background: [/^#(0x)?[0-9a-f]+$/i, /^[a-z]+$/i],
        'text-align': [/^left$/, /^right$/, /^center$/],
      },
    },
    // We don't currently allow img itself by default, but this
    // would make sense if we did
    // img: ['src']
  })
}
